# Propuesta uso de GIT
## Descripción
Se plantea una propuesta de uso de GIT con el fin de mantener lo más limpio posible el historico de la ramas principales de trabajo a fin de facilitar su visualización y desplazamientos entre las distintas tareas que se lleven a cabo durante el desarrollo.

### Estructura
Partimos, tal y como se ha acordado, de una rama `ENP-master`, desde la cual crearemos una `ENP-dev` donde intregraremos cada una de las ramas que se creen para los Sprints: `ENP-release-candidate-v0.1.0` ( ENP-sprint-1 o como sea ).

## Flujo de trabajo
En el momento en que nos asignan una tarea, la propuesta implica el siguiente flujo de trabajo.

1. Actualizaremos en local la rama del sprint en el que estamos. 

``` 
(ENP-master) $ git fetch --prune
(ENP-master) $ git checkout ENP-release-v0.1.0
```

2. Crearemos una rama para __desarrollo__ a partir de la del sprint actual en la que codificar nuestra tarea y que podremos trabajar como queramos.
```
(ENP-release-candidate-v0.1.0) $ git checkout -b ENP-dev-PNENP-18
```

3. En esta rama haremos todos los commits que queramos al igual que los merges de la rama de sprint que vayamos creyendo oportunos hacer
```
(ENP-dev-PNENP-18) $ git add -A
(ENP-dev-PNENP-18) $ git commit -m 'separación de ficheros'
(ENP-dev-PNENP-18) $ git commit -am 'modifico los estilos porque me gustan mas en rosa'
(ENP-dev-PNENP-18) $ git commit -a --amend --no-edit
(ENP-dev-PNENP-18) $ git fetch
(ENP-dev-PNENP-18) $ git merge ENP-release-candidate-v0.1.0
(ENP-dev-PNENP-18) $ git add .
(ENP-dev-PNENP-18) $ git commit -m 'creo los tests'
(ENP-dev-PNENP-18) $ git commit -am 'modifico los test y añado funcionalidad para header'
(ENP-dev-PNENP-18) $ git push origin ENP-dev-PNEPN-18
(ENP-dev-PNENP-18) $ git commit -am 'por fin termino mi tarea sin haber actualizado mi rama'

```
>  nota: en el ultimo commit, cuando digo que no he actualizado mi rama me refería a que no he mergeado con los nuevos cambios que otros compañeros han hecho en la rama del sprint

4. Ya he terminado mi tarea y ahora quiero llevarla a la rama del sprint. Mi log es el siguiente:
```
commit 902e6127bcbfd861f3274c9cff6e76b991a406b3 (HEAD -> ENP-dev-PNEPN-18)
Author: Sergio Diego Pérez García <sperez@clb.t-prisa.com>
Date:   Thu Mar 18 09:35:50 2021 +0100

    por fin termino mi tarea sin haber actualizado mi rama

commit 97958d8033ba2b32808ce37a787268e6088c1c44 (origin/ENP-dev-PNEPN-18)
Author: Sergio Diego Pérez García <sperez@clb.t-prisa.com>
Date:   Thu Mar 18 09:27:38 2021 +0100

    modifico los test y añado funcionalidad para header

commit c9bf664e71337a9753594ccb2cb3cb10a6add525
Author: Sergio Diego Pérez García <sperez@clb.t-prisa.com>
Date:   Thu Mar 18 09:26:15 2021 +0100

    creo los tests

commit dbea262316e7c1d61421755a0bc557991df8c813
Merge: 78f559e e91ec56
Author: Sergio Diego Pérez García <sperez@clb.t-prisa.com>
Date:   Thu Mar 18 09:22:26 2021 +0100

    Merge branch 'ENP-release-candidate-v0.1.0' into ENP-dev-PNEPN-18

commit e91ec569f5d07bbefe3a6e57e55d849e587725b8
Author: Sergio Diego Pérez García <sperez@clb.t-prisa.com>
Date:   Thu Mar 18 09:21:25 2021 +0100

    [PNEPN-20] Tarea de otra persona que no soy yo

commit 78f559e7b57a986f5564146aff1d24628187df28
Author: Sergio Diego Pérez García <sperez@clb.t-prisa.com>
Date:   Thu Mar 18 09:17:29 2021 +0100

    modifico los estilos porque me gustan mas en rosa

commit eb5ec890d6807ddbadcefd5607d41de82d756591
Author: Sergio Diego Pérez García <sperez@clb.t-prisa.com>
Date:   Thu Mar 18 09:15:45 2021 +0100

    separacion de ficheros

commit b13127335da07ac2850cd7007014d8ea8b90c70f (origin/master, origin/ENP-master, origin/ENP-dev, master, ENP-master, ENP-dev)
Author: Sergio Diego Pérez García <sperez@clb.t-prisa.com>
```

5. Antes de llevar mi tarea a la rama ENP-release-candidate-v0.1.0 tengo que hacer un pull de los cambios de mis compañeros y mergearlos con mi rama. (que cada uno lo haga como quiera, muestro el modo más simple para que se entienda fácilmente)
```
(ENP-dev-PNENP-18) $ git checkout ENP-release-candidate-v0.1.0
(ENP-release-candidate-v0.1.0) $ git pull origin ENP-release-candidate-v0.1.0
(ENP-release-candidate-v0.1.0) $ git checkout ENP-dev-PNENP-18
(ENP-dev-PNENP-18) $ git merge ENP-release-candidate-v0.1.0 

-----

$ git log

commit 3b4fb41123d183ab29dc61d36668973864cfe95d (HEAD -> ENP-dev-PNEPN-18)
Merge: e910ede 1d37dc8
Author: Sergio Diego Pérez García <sperez@clb.t-prisa.com>
Date:   Thu Mar 18 10:16:15 2021 +0100

    Merge branch 'ENP-release-candidate-v0.1.0' into ENP-dev-PNEPN-18

commit e910ede0981337ec2212d4f32c2896a53b494cf9
Author: Sergio Diego Pérez García <sperez@clb.t-prisa.com>
Date:   Thu Mar 18 09:35:50 2021 +0100

    por fin termino mi tarea sin haber actualizado mi rama
```

6. En este punto surgen dos posibilidades dependiendo de la decisión que tomemos sobre si mantener el histórico de commmits en ramas de desarrollo para 'facilitar' la localización de bugs o no.  

    a) En caso de querer mantener la rama de desarrollo (opción utilizada durante el tutorial) crearemos una nueva rama de nombre PNENP-18 en la que haremos la limpieza del historico y mezclaremos con la rc0.1.0. Voy a seguir esta opción en el ejemplo.
    
    b) En caso de no querer mantener la rama de desarrollo, todo lo que hemos hecho sobre la rama ENP-dev-PNENP-18 podríamos haberlo hecho directamente sobre PNENP-18 y en este punto hacer la limpieza.

    | _importante: en caso de tomar la opción b) siempre que hayamos hecho push al repo en algun momento no podremos hacer otro push a origin si no hemos borrado del repo previamente la rama PNENP-18 (`git push origin :ENP-PNENP-18`)_

7. Como hemos optado por mantener la rama de desarrollo en el ejemplo, la subiré al repo en primer lugar y luego haré las acciones necesarias para crear una nueva rama limpia que llevaremos.
```
(ENP-dev-PNENP-18) $ git push origin ENP-dev-PNEPN-18
(ENP-dev-PNENP-18) $ git checkout -b ENP-PNEP-18
(ENP-PNENP-18) $ git log

----
commit db05ca94b860a5afff8aa09b030a9864b9535bb8 (HEAD -> ENP-PNEP-18, origin/ENP-dev-PNEPN-18, ENP-dev-PNEPN-18)
Merge: e910ede 1d37dc8
Author: Sergio Diego Pérez García <sperez@clb.t-prisa.com>
Date:   Thu Mar 18 10:16:15 2021 +0100

    Merge branch 'ENP-release-candidate-v0.1.0' into ENP-dev-PNEPN-18

[...]

commit eb5ec890d6807ddbadcefd5607d41de82d756591
Author: Sergio Diego Pérez García <sperez@clb.t-prisa.com>
Date:   Thu Mar 18 09:15:45 2021 +0100

    separacion de ficheros

commit b13127335da07ac2850cd7007014d8ea8b90c70f (origin/master, origin/ENP-master, origin/ENP-dev, master, ENP-master, ENP-dev)
Author: Sergio Diego Pérez García <sperez@clb.t-prisa.com>
Date:   Thu Mar 18 08:41:49 2021 +0100

    añadido README.md
```

8. Analizando el log podemos ver el hash del commit del que partimos y lo utilizaremos para hacer el rebase. Si bien podemos utilizar rebase de muchas maneras pondré un ejemplo, tan válido como cualquiera, que creo que hace más fácil su entendimiento.
```
(ENP-PNENP-18) $ git rebase -i b13127335da07ac2850cd7007014d8ea8b90c70f

----
// aparecerá un editor similar a este (observad que dejo el primer commit como pick y los demás como squash pero se puede hacer todo lo que te de la gana de acuerdo a las opciones que tiene)

pick eb5ec89 separacion de ficheros
s 78f559e modifico los estilos porque me gustan mas en rosa
s 65b78d5 [PNEPN-20] Tarea de otra persona que no soy yo
s d899c29 creo los tests
s 013748c modifico los test y añado funcionalidad para header
s 3d41713 por fin termino mi tarea sin haber actualizado mi rama
s 86d3117 [PNENP-12] Otra tarea de otra persona que no soy yo
s 456ddd8 [PNENP-33] Mas tareas de gente

# Rebase b131273..456ddd8 en 456ddd8 (8 comandos)
#
# Comandos:
# p, pick <commit> = usar commit
# r, reword <commit> = usar commit, pero editar el mensaje de commit
# e, edit <commit> = usar commit, pero parar para un amend
# s, squash <commit> = usar commit, pero fusionarlo en el commit previo
# f, fixup <commit> = como "squash", pero descarta el mensaje del log de este commit
# x, exec <commit> = ejecuta comando ( el resto de la línea) usando un shell
# b, break = parar aquí (continuar rebase luego con 'git rebase --continue')
# d, drop <commit> = eliminar commit
# l, label <label> = poner label al HEAD actual con un nombre
# t, reset <label> = reiniciar HEAD a el label
# m, merge [-C <commit> | -c <commit>] <label> [# <oneline>]
# .       crea un commit de fusión usando el mensaje original de
# .       fusión (o la línea de oneline, si no se especifica un mensaje
# .       de commit). Use -c <commit> para reescribir el mensaje del commit.
#
# Estas líneas pueden ser reordenadas; son ejecutadas desde arriba hacia abajo.
#
# Si eliminas una línea aquí EL COMMIT SE PERDERÁ.
#
# Como sea, si quieres borrar todo, el rebase será abortado.
#
# Tenga en cuenta que los commits vacíos están comentados

-----

// a continuación aparecerá tu editor por defecto para que añadas un nombre al commit. Al ser una composición de distintos commits te saldrá el nombre de cada uno de ellos. Podrás modificar el mensaje del commit como quieras: dejandolo tal cual para cambiarlo luego (opción que tomo en el ejemplo aunque en realidad suelo dejar ya su nombre final), modificando para dejar el nombre definitivo, comentar los commits (sus mensajes) que no quieras que aparezcan, etc.

// otra opción para facilitar esto es haber hecho un fixup en lugar de un squash sobre los commits. Decisión de cada cual.

---

(ENP-PNENP-18) $ git log

commit ca087dcf7f9835681edd105004aeb21ee75e7313 (HEAD -> ENP-PNEP-18)
Author: Sergio Diego Pérez García <sperez@clb.t-prisa.com>
Date:   Thu Mar 18 09:15:45 2021 +0100

    separacion de ficheros
    
    modifico los estilos porque me gustan mas en rosa
    
    [PNEPN-20] Tarea de otra persona que no soy yo
    
    creo los tests
    
    modifico los test y añado funcionalidad para header
    
    por fin termino mi tarea sin haber actualizado mi rama
    
    [PNENP-12] Otra tarea de otra persona que no soy yo
    
    [PNENP-33] Mas tareas de gente

commit b13127335da07ac2850cd7007014d8ea8b90c70f (origin/master, origin/ENP-master, origin/ENP-dev, master, ENP-master, ENP-dev)
Author: Sergio Diego Pérez García <sperez@clb.t-prisa.com>
Date:   Thu Mar 18 08:41:49 2021 +0100

---

// ahora le voy a cambiar el mensaje al commit para adecuarlo a la tarea

(ENP-PNENP-18) $ git commit --amend

// se abrirá el editor por defecto en el que cambio el mensaje del commit y comento todos los demás commits (si quiero. tambien los puedo eliminar)

---

[PNENP-18] termino mi tarea en un solo commit

# separacion de ficheros
# modifico los estilos porque me gustan mas en rosa
# [PNEPN-20] Tarea de otra persona que no soy yo
# creo los tests
# modifico los test y añado funcionalidad para header
# por fin termino mi tarea sin haber actualizado mi rama
# [PNENP-12] Otra tarea de otra persona que no soy yo
# [PNENP-33] Mas tareas de gente

# Por favor ingresa el mensaje del commit para tus cambios. Las
#  líneas que comiencen con '#' serán ignoradas, y un mensaje
#  vacío aborta el commit.
#
# Fecha:     Thu Mar 18 09:15:45 2021 +0100
#
# En la rama ENP-PNEP-18
# Cambios a ser confirmados:
#       modificado:     README.md
#       nuevo archivo:  config.js
#       nuevo archivo:  src/body.js
#       nuevo archivo:  src/fichero1.js
#       nuevo archivo:  src/fichero2.js
#       nuevo archivo:  src/footer.js
#       nuevo archivo:  src/styles.css
#       nuevo archivo:  src/tests.js
#

---
(ENP-PNENP-18) $ git log

commit 1bb877375399b3ba8f9ba2083a297ae36e530958 (HEAD -> ENP-PNEP-18)
Author: Sergio Diego Pérez García <sperez@clb.t-prisa.com>
Date:   Thu Mar 18 09:15:45 2021 +0100

    [PNENP-18] termino mi tarea en un solo commit

commit b13127335da07ac2850cd7007014d8ea8b90c70f (origin/master, origin/ENP-master, origin/ENP-dev, master, ENP-master, ENP-dev)
Author: Sergio Diego Pérez García <sperez@clb.t-prisa.com>
Date:   Thu Mar 18 08:41:49 2021 +0100

    añadido README.md

```

9. Ahora ya tengo un rama con un único commit que resuelve mi tarea. Lo subiré al repo para poder hacer el merge-request (sería interesante que se hicieran los MR como cherry-pick para evitar el mensaje de "branch X merged in Y" pero eso ya es harina de otro costal). 
```
(ENP-PNENP-18) $ git push origin ENP-PNENP-18

```

## Información adicional sobre siguientes pasos en el flujo de trabajo
- En mi ejemplo, y teniendo en cuenta que tengo permisos en la rama RC, he llevado mi commit de la tarea de manera limpia con un cherry-pick
``` 
(ENP-release-candidate-v0.1.0) $ git cherry-pick f6972692c01d8317b2dcac863c929af36a6309be
```
- A continuación hice el push
``` 
(ENP-release-candidate-v0.1.0) $ git push origin ENP-release-candidate-v0.1.0
```
- Si ahora hacemos un git log ```(ENP-release-candidate-v0.1.0) $ git log``` vemos que la rama RC está completamente limpia
``` 
commit 136f68653447e6672557b5b142268e0442c694d5 (HEAD -> ENP-release-candidate-v0.1.0, origin/ENP-release-candidate-v0.1.0)
Author: Sergio Diego Pérez García <sperez@clb.t-prisa.com>
Date:   Thu Mar 18 09:15:45 2021 +0100

    [PNENP-18] termino mi tarea en un solo commit

commit 1d37dc8993c6e43c275fc42e495ac892788734c7
Author: Sergio Diego Pérez García <sperez@clb.t-prisa.com>
Date:   Thu Mar 18 09:30:53 2021 +0100

    [PNENP-33] Mas tareas de gente

commit 24eb6c1df65b21638775a9411a9dac4436a6747a
Author: Sergio Diego Pérez García <sperez@clb.t-prisa.com>
Date:   Thu Mar 18 09:29:23 2021 +0100

    [PNENP-12] Otra tarea de otra persona que no soy yo

commit e91ec569f5d07bbefe3a6e57e55d849e587725b8
Author: Sergio Diego Pérez García <sperez@clb.t-prisa.com>
Date:   Thu Mar 18 09:21:25 2021 +0100

    [PNEPN-20] Tarea de otra persona que no soy yo

commit b13127335da07ac2850cd7007014d8ea8b90c70f (origin/master, origin/ENP-master, origin/ENP-dev, master, ENP-master, ENP-dev)
Author: Sergio Diego Pérez García <sperez@clb.t-prisa.com>
Date:   Thu Mar 18 08:41:49 2021 +0100

    añadido README.md
```
- Una vez terminado el sprint mergeamos la RCv0.1.0 con DEV... y a seguir. A partir de aquí, incluso un poco antes, no nos incumbe a los desarrolladores.